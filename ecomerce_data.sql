Use 24h_dev;

/*customer*/
INSERT INTO customer VALUES (1001,'Leo','Messi','2000-10-10','M','leo1234',NULL,NULL,NULL,NULL),		
(1002,'Cristiano','Ronaldo','2000-7-7','M','cr7777',NULL,NULL,NULL,NULL),
(1003,'Neymar','Junior','2000-10-6','M','neymaryoungboi',NULL,NULL,NULL,NULL),
(1004,'Kilian','Mbappe','1998-8-16','M','goldenboi2018',NULL,NULL,NULL,NULL),
(1005,'Will','Smith','1996-8-19','M','maninblack',NULL,NULL,NULL,NULL),
(1006,'Justin','Bieber','200-8-19','M','hitofmalesinger',NULL,NULL,NULL,NULL);

INSERT INTO phone VALUES (10001,1001,84,2836774),(10002,1001,84,281010),
(10003,1002,84,107777),(10004,1003,84,232323),(10005,1003,23,101010),
(10006,1004,84,712000),(10007,1005,84,858512),(10008,1006,84,856712);

INSERT INTO email VALUES (10001,1001,'leo_barca@gmail.com'),
(10002,	1001,'leo_PSG@macrosoft.com'),
(10003,	1002,'cr7_MU@hotmail.com'),
(10004,	1003,'njr_PSG@urw.edu'),
(10005,	1004,'MBP_PSG@gmail.com'),
(10007,	1005,'Smithoffical@gmail.com'),
(10008,	1006,'JB72@gmail.com');

INSERT INTO address VALUES (10001,1001,'7028 Pasc de Prince','Paris',11717,'France'),
(10002,1002,'9114 S. Queen Road','Manchester',12123,'England'),
(10003,1002,'82 Roatioze','Lisbon',33125,'Portugal'),
(10004,1003,'35 Goldfield Drive','Paris',11717,'France'),
(10005,1003,'34 W. James Dr','Rio',78912,'Brazi'),
(10006,1004,'7086 Pasc de Prince','Paris',11717,'France'),
(10007,1005,'68 Ohio Street','Ohio',30126,'USA'),
(10008,1005,'88 LA Street','Long beach',30126,'USA');

INSERT INTO payment VALUES 
(100001,1001,'368544623795203','Visa'),
(100002,1002,'4937515149549500','Visa'),
(100003,1003,'4808437630180081','Visa'),
(100004,1004,'4828526348154572','Visa'),
(100005,1005,'4248168108403535','Visa'),
(100006,1006,'4223456712098120','Visa');

/*Product*/
INSERT INTO product VALUES (100,'predator 2.0',200,1,'New version','All color','All size',NULL,NULL,150,NULL,NULL,180),
(101,'MU home`s KITs',200,1,'New season kit','Red','All size',NULL,NULL,800,NULL,NULL,80),
(102,'PSG home`s KITs',201,1,'New season kit','Deep blue','All size',NULL,NULL,550,NULL,NULL,90),
(103,'RonaldoSpecial Boost',201,1,'The limmited for CR7','golden-white','43,44,45',NULL,NULL,30,NULL,NULL,1200),
(104,'orika Smart TV',202,4,'The new mordern','Black,deep blue','60 inch',NULL,NULL,15,NULL,NULL,600),
(105,'SS Galaxy2021',202,4,'model of 2021','black,red','6.8 inch',NULL,NULL,300,NULL,NULL,150),
(106,'fall season Hoodie',204,2,'the best seller','Black','All size',NULL,NULL,10,NULL,NULL,1000),
(107,'Iphone 13',203,4,'just the 2021 IP','All color','6.8 inch',NULL,NULL,15,NULL,NULL,1600),
(108,'Blue de channel',205,3,'Perfume for man','only one','medium',NULL,NULL,4,NULL,NULL,800);


INSERT INTO supplier VALUES (200,'Adidas','USA','adidasoffical@gmail.com',NULL),
(201,'NIKE','USA','offical@gmail.nike.com',NULL),
(202,'SAMSUM','KOREA','samsum@gmail.com',NULL),
(203,'APPLE','USA','theoffical@gmail.appleworldwide.com',NULL),
(204,'FearOfGod','USA','fearofgod@gmail.com',NULL),
(205,'Chanel','France','officalchanel@gmail.com',NULL);

INSERT INTO category VALUES (1,'SPORTS','All of sports equipment like (shoes,merch...) '),
(2,'Fashion','shirt,pant,blink,...'),
(3,'Perfume','Special perfume of all special Brands around the world'),
(4,'Electronic device','All of its')
;





