Use 24h_dev;


CREATE TABLE customer (
	cust_id INT PRIMARY KEY,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	dob DATE NOT NULL,
	gender VARCHAR(1) NOT NULL,
    password VARCHAR(12) NOT NULL,
	address_id INT,
	phone_id INT,
	email_id INT,
	payment_id INT
    );

CREATE TABLE phone (
phone_id INT PRIMARY KEY,
cust_id INT NOT NULL,
country_code INT NOT NULL,
phone_number INT NOT NULL,
FOREIGN KEY(cust_id) REFERENCES customer(cust_id) ON DELETE CASCADE
);

CREATE TABLE email (
email_id INT PRIMARY KEY,
cust_id INT NOT NULL,
email VARCHAR(254) NOT NULL,
FOREIGN KEY(cust_id) REFERENCES customer(cust_id) ON DELETE CASCADE
);

CREATE TABLE address (
address_id INT PRIMARY KEY,
cust_id INT NOT NULL,
address VARCHAR(95) NOT NULL,
city VARCHAR(35) NOT NULL,
zip_code INT NOT NULL,
country VARCHAR(74) NOT NULL,
FOREIGN KEY(cust_id) REFERENCES customer(cust_id) ON DELETE CASCADE
);

CREATE TABLE payment (
payment_id INT PRIMARY KEY,
cust_id INT NOT NULL,
creditcard_number VARCHAR(16) NOT NULL,
creditcard_type VARCHAR(20) NOT NULL,
FOREIGN KEY(cust_id) REFERENCES customer(cust_id) ON DELETE CASCADE
);

CREATE TABLE product (
product_id INT PRIMARY KEY,
product_name VARCHAR(50) NOT NULL,
supplier_id INT NOT NULL,
category_id INT NOT NULL, 
description VARCHAR(95),
availablecolor VARCHAR(20),
availablesize VARCHAR(20),
max_discount INT,
discount INT,
UnitinStock INT,
UnitinOrder INT,
ranking INT,
price_per_unit INT NOT NULL,
FOREIGN KEY(supplier_id) REFERENCES supplier(supplier_id) ON DELETE CASCADE,
FOREIGN KEY(category_id) REFERENCES category(category_id) ON DELETE CASCADE
);

CREATE TABLE supplier (
supplier_id INT NOT NULL PRIMARY KEY,
company_name VARCHAR(50) NOT NULL,
address VARCHAR(95) NOT NULL,
email VARCHAR(254) NOT NULL,
phone_number INT
);



CREATE TABLE category (
category_id INT NOT NULL PRIMARY KEY,
category_name VARCHAR(50),
description VARCHAR(95)
);

CREATE TABLE oder(
oder_id INT PRIMARY KEY,
cust_id INT NOT NULL,
quantity INT NOT NULL,
discount INT,
orderdate DATE,

FOREIGN KEY(cust_id) REFERENCES customer(cust_id) ON DELETE CASCADE
);

CREATE TABLE order_details (
oder_id INT,
product_id INT,
payment_date DATE,
order_date DATE NOT NULL,
ship_date DATE,
ship_method VARCHAR(30) NOT NULL,
unitinOder INT,
return_date DATE,
FOREIGN KEY(product_id) REFERENCES product(product_id) ON DELETE CASCADE,
FOREIGN KEY(oder_id) REFERENCES oder(oder_id) ON DELETE CASCADE
);

ALTER TABLE product
ADD FOREIGN KEY(category_id)
REFERENCES category(category_id)
ON DELETE CASCADE;


ALTER TABLE customer
ADD FOREIGN KEY(email_id)
REFERENCES email(email_id)
ON DELETE CASCADE;

ALTER TABLE customer
ADD FOREIGN KEY(phone_id)
REFERENCES phone(phone_id)
ON DELETE CASCADE;

ALTER TABLE customer
ADD FOREIGN KEY(address_id)
REFERENCES address(address_id)
ON DELETE CASCADE;






